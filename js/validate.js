﻿var nameObj;
var wordObj;
var confirm_wordObj;

var submitButton;

var nameInfo;
var wordInfo;
var confirm_wordInfo;

var flag1 = new Boolean(true);
var flag2 = new Boolean(true);
var flag3 = new Boolean(true);

window.onload=function()
{	
	nameObj=document.getElementById("name");
	wordObj=document.getElementById("word");
	confirm_wordObj=document.getElementById("confirm_word");
	
	nameInfo=document.getElementById("name_info");
	wordInfo=document.getElementById("word_info");
	confirm_wordInfo=document.getElementById("confirmword_info");
	
	submitButton=document.getElementById("submit");

	successElement=document.getElementById("registration-success");
	success1Element=document.getElementById("login-success");
	
	function checkName()
	{		
		var msg="";
		if(nameObj.value.length>10||nameObj.value.length<3) {msg="用户名长度为3-10";flag1=false;}
		else flag1=true;
		if(!nameObj.value) {msg="用户名必须填写";flag1=false;}
		nameInfo.innerHTML=msg;
		
		return msg=="";
	}

	function checkWord()
	{
		var msg="";
		if(wordObj.value.length>20||wordObj.value.length<6) {msg="密码长度为6-20";flag2=false;}
		else flag2=true;
		if(!wordObj.value) {msg="密码必须填写";flag2=false;}
		wordInfo.innerHTML=msg;
		
		return msg=="";
	}

	function checkconfirmWord()
	{
		var msg="";
		if(confirm_wordObj.value!=wordObj.value) {msg="密码不一致";flag3=false;}
		else flag3=true;
		confirm_wordInfo.innerHTML=msg;
		
		return msg=="";
	}
	
	submitButton.onclick=function()
	{			
		var bName=checkName();
		var bWord=checkWord();
		var bconfirmWord=checkconfirmWord();
		
		console.log(flag1)
		console.log(flag2)
		if(flag1&&flag2&&flag3) {successElement.style.display="block";nameObj.value="";wordObj.value="";confirm_wordObj.value="";}
	}
};